// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
// To understand behaviors, see https://drupal.org/node/756722#behaviors
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.rsc_taxonomy = {
  attach: function(context, settings) {

    // find the menus on the page (assuming all blocks made by this module are menus)
    $(".block-rsc-taxonomy > div.content").each(function(index){

      // Get the current menu's outer div
      var menu_div = $(this);

      // Get the vid of the vocabulary that this menu is based on
      var vid = menu_div.attr("vid");

      // Determine the url to load the menu from if necessary
      var menu_url = Drupal.settings.basePath + "rsc_taxonomy_menu/" + vid;

      // Determine what we call our stuff in localstorage
      var markup_id = "rsc_taxonomy_menu_markup_" + vid;
      var modified_id = "rsc_taxonomy_menu_modified_" + vid;

      // Show that something is happening!
      menu_div.html("Loading ...");

      // check if localstorage is supported by this browser
      var support = false;
      try {
        if ('localStorage' in window && window['localStorage'] !== null) {
          support = true
        }
      } catch (e) {}

      // if localstorage is supported, try to load the menu from the localstorage
      if (support) {

        var markup = localStorage[markup_id];
        var modified = localStorage[modified_id];

        if (markup && modified && (menu_div.attr("modified") == modified)) {
          //console.log("cache hit");

          // add the menu to the DOM
          menu_div.html(markup);

        } else {
          //console.log("cache miss");

          // request the menu from the server using ajax
          $.ajax({
            url: menu_url,
            type: 'get',
            dataType: 'html',
            async: true,
            success: function(data) {
              markup = data;

              // save the menu to localstorage for next time
              localStorage[markup_id] = markup;
              localStorage[modified_id] = menu_div.attr("modified");

              // add the menu to the DOM
              menu_div.html(markup);
            }
          });

        }

      } else {
        // localstorage is not supported. Just get the menu from the server.
        menu_div.load(menu_url);
      }

    })

  }
};


})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/

/**
 * Adds the autocomplete widget behavior to specified elements.
 *
 * This uses the jQuery UI Autocomplete widget included with Drupal core.
 */
Drupal.apachesolr_autocomplete = {
  processOne: function(key, settings, context) {
    // Look for items with the data-apachesolr-autocomplete-id attribute.
    var jquery_version = jQuery.fn.jquery.split('.');
    var apachesolr_autocomplete_selector = ((jquery_version[0] == 1 && jquery_version[1] >= 7) || jquery_version[0] > 1) ? 'ui-autocomplete' : 'autocomplete';
    var apachesolr_autocomplete_search = jQuery(".apachesolr-autocomplete[data-apachesolr-autocomplete-id='"+ key +"']", context);
    apachesolr_autocomplete_search.autocomplete({
        // TODO: source should be a function, which should add any client-side filters to autocomplete request.
        source: settings.path,
        // TODO: autocomplete select event should handle more actions on select: filling/submitting the textfield, jumping to URL... others?
        select: function(event, ui) {
            console.log("autocomplete.select: " + ui.item.value); //TODO: Debug
            // ui.item.label, ui.item.value, ui.item.count, etc.
            // Handle selection of an element in the autocomplete widget.
            jQuery(this).get(0).value = ui.item.value;
            // We should submit the widget's parent form.
            jQuery(this).closest("form").submit()
        },
        search: function(event, ui) {
            jQuery(this).addClass('throbbing');
        },
        // The following doesn't fire--why??
        response: function( event, ui ) {
            console.log('autoccomplete.response fired');
            jQuery(this).removeClass('throbbing');
        },
        open: function(event,ui) {
            jQuery(this).removeClass('throbbing');
        },
        minLength: 2,
        delay: 400
    })
    .addClass('form-autocomplete');
    if (apachesolr_autocomplete_search.data(apachesolr_autocomplete_selector)) {
      apachesolr_autocomplete_search.data(apachesolr_autocomplete_selector)._renderItem = Drupal.apachesolr_autocomplete.renderItem;
    }
  },
    // TODO: USe JS-side theming. See http://engineeredweb.com/blog/11/5/javascript-theme-functions-drupal/
  renderItem: function (ul, item) {
      var theme_function = 'apachesolr_autocomplete_default';
      if (item.theme) {
          theme_function = item.theme;
      }
      theme_function = theme_function;
      var html = Drupal.theme(theme_function, item);
      return jQuery("<li>")
          .data("item.autocomplete", item)
          .append(html)
          .appendTo(ul);
  }
};

/**
 * Implements the Drupal behavior functions that trigger on page load.
 *
 * Uses the JS Drupal.settings.apachesolr_autocomplete object set by the
 * apachesolr_autocomplete module to change any relevant form elements currently
 * on the page to be autocomplete-enabled.
 */
Drupal.behaviors.apachesolr_autocomplete = {
  attach: function(context) {
    // Check that settings exist.
    if (!Drupal.settings.apachesolr_autocomplete || !Drupal.settings.apachesolr_autocomplete.forms) {
      return;
    }
    // Cycle thru the settings array which contains:
    //   key => settingsArray
    // where
    //   key: the data-apachesolr-autocomplete-id HTML attribute to look for
    //        (which was added on the form_alter)
    //   settings: and
    jQuery.each(Drupal.settings.apachesolr_autocomplete.forms, function(key, settings) {
      Drupal.apachesolr_autocomplete.processOne(key, settings, context);
    });
  }
};

/**
 * Default theming function.
 * @param item
 *   The item object.
 * @returns {string}
 */
Drupal.theme.prototype.apachesolr_autocomplete_default = function(item) {
    return item.label;
};

;/*})'"*/
;/*})'"*/
(function ($) {
  Drupal.behaviors.rsc_attachment_formatter_dropdown = {
    attach: function (context, settings) {
      var $ul = $('ul.rsc-attachment-formatter-list');
      $ul.addClass('js');
      $('li.dl, li.drive',$ul).wrapAll("<div class='drop' />");
      var $drop = $('div.drop',$ul);
      
      $drop.hide();
      $('li.read',$ul).append("<a href='#/' class='more'>&darr;</a>");
      $('a.more',$ul).click(function(e){
        $drop.toggle(200);
        $ul.toggleClass('open');
        e.stopPropagation();
      });
      
      $("body").click(function(){
        $drop.hide(200);
      });
    }
  };
}(jQuery));

;/*})'"*/
;/*})'"*/
